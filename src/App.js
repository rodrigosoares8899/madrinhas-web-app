import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home';
import MenuBar from "./components/MenuBar";
import Payments from "./components/Payments";
import PaymentForm from "./pages/PaymentForm";
import myTheme from "./Theme";
import {ThemeProvider} from "@material-ui/core/styles";
import Bridesmaid from "./pages/Bridesmaid";


class App extends Component {
  render() {
    return (
      <ThemeProvider theme={myTheme}>
      <BrowserRouter>
        <MenuBar/>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path='/home' component={Home}/>
          <Route exact path='/payment' component={PaymentForm}/>
          <Route exact path='/bridesmaid/:id' component={Bridesmaid}/>
        </Switch>
      </BrowserRouter>
      </ThemeProvider>

    );
  }
}

export default App;
