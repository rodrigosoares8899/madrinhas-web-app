import React from 'react';
import orange from '@material-ui/core/colors/orange';
import lightGreen from '@material-ui/core/colors/lightGreen';
import green from '@material-ui/core/colors/green';

import {createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: orange[700]
    },
    secondary: {
      main: lightGreen[500],
    },
    success: {
      main: green[700],
    }
  },
  typography: {
    button: {
      color: "#FFF"
    },
  }
});


export default theme