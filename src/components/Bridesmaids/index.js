import React, {useEffect, useState} from "react";
import {makeStyles} from '@material-ui/core/styles';
import api from "../../shared/api";
import Skeleton from '@material-ui/lab/Skeleton';
import blue from "@material-ui/core/colors/blue";
import {Dialog, DialogTitle, ListItemAvatar, List, ListItem, Avatar, ListItemText} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import {useHistory, useLocation} from "react-router-dom";

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

export default function Bridesmaids(props) {
  const classes = useStyles();
  const {open, onClose} = props;
  const [bridesmaids, setBridesmaids] = useState();
  const [loading, setLoading] = useState(true);

  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    async function getBridesmaids() {
      const response = await api.get('/bridesmaids');
      setBridesmaids(response.data);
      setLoading(false);
    }

    getBridesmaids();

  }, [])

  const handleListItemClick = (value) => {
    let { from } = location.state || { from: { pathname: "/bridesmaid/"+value } };
    history.replace(from);
    onClose();
  };

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog aria-labelledby="simple-dialog-title" open={open} onClose={handleClose}>
      <DialogTitle id="simple-dialog-title">Selecione a madrinha</DialogTitle>
      {
        loading ?
          <Skeleton/> :
          <List>
            {bridesmaids.map((bridesmaid) => (
              <ListItem button onClick={() => handleListItemClick(bridesmaid.id)}>
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <PersonIcon/>
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={bridesmaid.name}/>
              </ListItem>
            ))}
          </List>
      }
    </Dialog>
  );

}