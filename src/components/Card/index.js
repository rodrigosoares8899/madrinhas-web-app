import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Card, CardMedia, CardActionArea, CardContent, Typography} from "@material-ui/core";
import Title from "../Title";

export default function DestinyCard(props) {
  const useStyles = makeStyles({
    root: {
      maxWidth: 300,
    },
    media: {
      height: 100,
    },
  });

  var img;

  switch (props.id) {
    case 1:
      img = 'https://www.remessaonline.com.br/blog/wp-content/uploads/2020/05/picpay.png';
      break;
    case 2:
      img = 'https://www.jornalcontabil.com.br/wp-content/uploads/2019/04/novo-logo-santander-fundo-vermelho-696x365.jpg';
      break;
    case 3:
      img = 'https://www.contabeis.com.br/assets/img/news/n_42524_35817bda28b111aa49bd8fdf61878246.jpg';
      break;
  }

  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={img}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.name}
          </Typography>
          <Typography variant="body2" component="p">
            {props.amount}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );

}