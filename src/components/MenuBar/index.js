import React, {useState} from "react";
import useStyles from "./styles";
import clsx from 'clsx';
import {ThemeProvider, useTheme} from "@material-ui/core/styles";
import {
  CssBaseline, AppBar, Toolbar,
  IconButton, Typography, Drawer, Divider,
  List, ListItem, ListItemIcon, ListItemText
}
  from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import myTheme from "../../Theme";
import {Link} from "react-router-dom";
import Bridesmaids from "../Bridesmaids";

const MenuBar = () => {

  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const [openBridesmaidsDialog, setOpenBridesmaidsDialog] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  }

  const handleDrawerClose = () => {
    setOpen(false);
  }

  const handleOnClickBridesmaids = () => {
    setOpenBridesmaidsDialog(!openBridesmaidsDialog);
  }

  const handleCloseBridesmaidsDialog = () => {
    setOpenBridesmaidsDialog(false);
  }

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <ThemeProvider theme={myTheme}>
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open
          })}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon/>
            </IconButton>
            <Typography variant="h6">Olá Bem-vindo</Typography>
          </Toolbar>
          <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
          >
            <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
              </IconButton>
            </div>
            <Divider/>
            <List>
              <Link to="/payments">
                <ListItem button selected={true} key="payments">
                  <ListItemIcon> <ChevronRightIcon/> </ListItemIcon>
                  <ListItemText primary="Pagamentos"/>
                </ListItem>
              </Link>
              <Link to="/payment">
                <ListItem button selected={true} key="new payment">
                  <ListItemIcon> <ChevronRightIcon/> </ListItemIcon>
                  <ListItemText primary="Novo Pagamento"/>
                </ListItem>
              </Link>
              <ListItem button selected={true} key="bridesmaids" onClick={handleOnClickBridesmaids}>
                <ListItemIcon> <ChevronRightIcon/> </ListItemIcon>
                <ListItemText primary="Madrinhas"/>
              </ListItem>
              <Bridesmaids open={openBridesmaidsDialog} onClose={handleCloseBridesmaidsDialog} />
            </List>
            <main className={clsx(classes.content, {
              [classes.contentShift]: open
            })}>
              <div className={classes.drawerHeader}/>
            </main>
          </Drawer>
        </AppBar>
      </ThemeProvider>
    </div>
  )
}

export default MenuBar;