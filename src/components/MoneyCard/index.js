import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";
import Skeleton from '@material-ui/lab/Skeleton';
import Title from "../Title";
import api from '../../shared/api';

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

export default function MoneyCard() {
  const classes = useStyles();
  const [ammount, setAmmount] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadAmmount() {
      const response = await api.get('/paymentsAmount');
      setAmmount(response.data);
      setLoading(false);
    }

    loadAmmount();
  }, [])

  return (
    <React.Fragment>
      {
        loading ? <Skeleton/> :
          <Title>Total Pago</Title>
      }
      <Typography component="p" variant="h4" className={classes.depositContext}>
        {loading ? <Skeleton/> : 'R$ '+ammount}
      </Typography>
    </React.Fragment>
  )
}