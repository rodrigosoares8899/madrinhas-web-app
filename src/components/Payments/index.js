import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from "../Title";
import usePayments from "./payments";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Payments() {

  const classes = useStyles();

  const payments = usePayments();

  return (
    <React.Fragment>
      <Title>Pagamentos</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Data</TableCell>
            <TableCell>Madrinha</TableCell>
            <TableCell>Destino</TableCell>
            <TableCell>Descrição</TableCell>
            <TableCell align="right">Valor</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {payments.map((payment) => (
            <TableRow key={payment.id}>
              <TableCell>{payment.created_at}</TableCell>
              <TableCell>{payment.user.name}</TableCell>
              <TableCell>{payment.destiny.name}</TableCell>
              <TableCell>{payment.description}</TableCell>
              <TableCell align="right">{payment.value}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <Link color="primary" href="#" onClick={preventDefault}>
          Veja mais
        </Link>
      </div>
    </React.Fragment>
  );
}