import {useEffect, useState} from "react";
import {getToken} from "../../shared/auth";
import api from "../../shared/api";

const usePayments = function(){
  const [payments, setPayments] = useState([]);

  useEffect(() => {
    async function loadPayments() {
      let token = getToken();

      const response = await api.get('/payments', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });

      setPayments(response.data);
    }

    loadPayments();

  }, []);

  return payments;
}

export default usePayments;