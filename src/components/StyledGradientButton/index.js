import React from 'react';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';
import orange from '@material-ui/core/colors/orange';
import lightGreen from '@material-ui/core/colors/lightGreen';

const StyledGradientButton = withStyles({
    root: {
      background: 'linear-gradient(45deg,'+orange[700]+' 30%, '+orange[500]+' 90%)',
      borderRadius: 3,
      border: 0,
      color: 'white',
      height: 48,
      padding: '0 30px',
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    },
    label: {
      textTransform: 'uppercase',
    },
  })(Button);

  export default StyledGradientButton;