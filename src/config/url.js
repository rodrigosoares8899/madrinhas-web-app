const BASE_URL = 'https://casamento-app.herokuapp.com';

const BASE_URL_API = `${BASE_URL}/api`;

export {BASE_URL, BASE_URL_API};