import app from 'firebase/app';
require('firebase/storage');
require('firebase/auth');
require('firebase/database');

let firebaseConfig = {
    apiKey: "AIzaSyAaR1h_XxcRFq00_WOs7iTDjaXqL35AxvI",
    databaseURL: "https://postos-ab5e7.firebaseio.com/",
};

class Firebase{
    constructor(){
        app.initializeApp(firebaseConfig);
    }

    login(email, password){
        return app.auth().signInWithEmailAndPassword(email, password);
    }

    async register(name, lastName, cpf, email, password){
        await app.auth().createUserWithEmailAndPassword(email, password);

        const uid = app.auth().currentUser.uid;

        return app.database()
            .ref('users')
            .child(uid)
            .set(
                {
                    name: name,
                    lastName: lastName,
                    cpf: cpf,
                }
            )
    }

    isInitialized(){
        return new Promise(resolve =>{
            app.auth().onAuthStateChanged(resolve);
        })
    }
}


export default new Firebase();