import React, {useEffect, useState} from "react";
import {useParams} from 'react-router-dom';
import {Avatar, Paper, Grid} from "@material-ui/core";
import useStyles from "./styles";
import PersonIcon from "@material-ui/icons/Person";
import Typography from "@material-ui/core/Typography";
import Title from "../../components/Title";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import api from "../../shared/api";
import Skeleton from '@material-ui/lab/Skeleton';
import CircularProgress from "@material-ui/core/CircularProgress";

const Bridesmaid = () => {

  let {id} = useParams();

  const classes = useStyles();

  const [bridesmaid, setBridesmaid] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getBridesmaid() {
      const response = await api.get(`bridesmaids/${id}`);
      setBridesmaid(response.data);
      setLoading(false);
    }

    getBridesmaid();
  }, [id])


  const payments = bridesmaid?.payments;

  return (
    <main className={classes.layout}>
      <Paper className={classes.paper}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Avatar>
              <PersonIcon/>
            </Avatar>
          </Grid>
          <Grid item={8}>
            {
              !loading ?
                <Typography variant="h4" gutterBottom>
                  {bridesmaid?.name}
                </Typography>
                :
                <CircularProgress />
            }

          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container spacing={3} style={{justifyContent: "center"}}>
          {
            !loading ?
              <Typography variant="h3" color="primary">
                Total: R$ {bridesmaid?.myAmount}
              </Typography>
              :
              <CircularProgress />
          }
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Title>Pagamentos</Title>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Data</TableCell>
              <TableCell>Destino</TableCell>
              <TableCell>Descrição</TableCell>
              <TableCell align="right">Valor</TableCell>
            </TableRow>
          </TableHead>
          {
            !loading
            ?
              <TableBody>
                {payments?.map((payment) => (
                  <TableRow key={payment.id}>
                    <TableCell>{payment.created_at}</TableCell>
                    <TableCell>{payment.destiny.name}</TableCell>
                    <TableCell>{payment.description}</TableCell>
                    <TableCell align="right">{payment.value}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
              :
              <CircularProgress />
          }
        </Table>
      </Paper>
    </main>
  )
}

export default Bridesmaid;