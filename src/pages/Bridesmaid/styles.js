import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";

const useStyles = makeStyles((theme) => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(12),
    [theme.breakpoints.up(600 + theme.spacing(1) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
  },
  contentTop: {
    display: "flex",
    justifyContent: "center",
  },
  avatar: {
    marginTop: theme.spacing(-4.5),
    position: "absolute",
    backgroundColor: blue[100],
    color: blue[600],
  }
}))

export default useStyles;