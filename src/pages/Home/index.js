import React, {useEffect, useState} from 'react';
import {Container, Grid, Paper} from "@material-ui/core";
import Skeleton from '@material-ui/lab/Skeleton';
import clsx from "clsx";
import useStyles from "./styles";
import MoneyCard from "../../components/MoneyCard";
import DestinyCard from "../../components/Card";
import api from "../../shared/api";
import Payments from "../../components/Payments";

export default function Home() {

  const classes = useStyles();

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  const [amountByDestiny, setAmountByDestiny] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getAmountByDestiny() {
      const response = await api.get('/paymentsAmountByDestniy');
      setAmountByDestiny(response.data);
      setLoading(false);
    }

    getAmountByDestiny();
  }, []);

  return (
    <main className={classes.content}>
      <div className={classes.appBarSpacer}/>
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>
          {/* Destinys */}
          <Grid item xs={12} md={8} lg={9}>
            {
              !loading ?
                <Paper className={fixedHeightPaper}>
                  <Container className={classes.cardsContent}>
                    {
                      amountByDestiny.map((amountByDestiny) => (
                        <Grid xs={12} md={3} lg={3}>
                          <DestinyCard
                            key={amountByDestiny.id}
                            id={amountByDestiny.id}
                            name={amountByDestiny.name}
                            amount={amountByDestiny.amount}/>
                        </Grid>
                      ))
                    }
                  </Container>
                </Paper> :
                <Skeleton/>
            }
          </Grid>
          {/* Total */}
          <Grid item xs={12} md={4} lg={3}>
            <Paper className={fixedHeightPaper}>
              <MoneyCard/>
            </Paper>
          </Grid>
          {/* Payments */}
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Payments/>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </main>
  )
}