import React, {useState, useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';
import Typography from '@material-ui/core/Typography';
import {makeStyles, ThemeProvider} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import myTheme from '../../Theme';
import StyledGradientButton from '../../components/StyledGradientButton';
import api from "../../shared/api";
import {setToken} from "../../shared/auth";
import {useHistory, useLocation} from "react-router-dom";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: myTheme.palette.primary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const classes = useStyles();
  const [name, setName] = useState();
  const [password, setPassword] = useState();
  const history = useHistory();
  const location = useLocation();

  async function doLogin() {
    //TODO loading
    try {
      let body = {name, password};
      let { from } = location.state || { from: { pathname: "/home" } };
      const response = await api.post('/login', JSON.stringify(body));
      if(response.status === 200 && response.data.api_key && response.data.role === 2){
        setToken(response.data.api_key);
        history.replace(from);
      }
    } catch (error) {
      //TODO controle de erro
      console.log(error);
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <Icon>account_box</Icon>
        </Avatar>
        <Typography variant="h5">Login</Typography>
        <form className={classes.form} onSubmit={
          event => {
            event.preventDefault();
            doLogin();
          }
        }>
          <ThemeProvider theme={myTheme}>
            <TextField
              variant="outlined"
              fullWidth={true}
              id="name"
              name="name"
              required
              margin="normal"
              label="Seu Nome"
              onChange={(e) => {
                setName(e.target.value)
              }}
            />
            <TextField
              variant="outlined"
              fullWidth={true}
              id="password"
              name="password"
              autoComplete="current-password"
              required
              margin="normal"
              label="Password"
              type="password"
              onChange={(e) => {
                setPassword(e.target.value)
              }}
            />

          </ThemeProvider>

          <StyledGradientButton
            className={classes.submit}
            type="submit"
            fullWidth={true}
            variant="contained"
            children="Fazer Login"
          />
        </form>
      </div>
    </Container>
  );
}