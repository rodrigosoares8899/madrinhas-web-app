import React, {useState, useEffect} from 'react';
import clsx from 'clsx';
import useStyles from "./styles";
import {
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Paper,
  Snackbar,
  TextField,
  Typography,
  Fab
} from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';
import SaveIcon from '@material-ui/icons/Save';
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/Cancel';
import usePaymentFormData from "./paymentFormData";
import api from "../../shared/api";
import auth from "../../shared/auth";
import {useHistory, useLocation} from "react-router-dom";

const PaymentForm = () => {

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const formData = usePaymentFormData();

  const classes = useStyles();

  const [value, setValue] = useState('');
  const [bridesmaid, setBridesmaid] = useState('');
  const [destiny, setDestiny] = useState('');
  const [description, setDescription] = useState('');
  const [buttonLoading, setButtonLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const history = useHistory();
  const location = useLocation();

  const buttonClassname = clsx({
    [classes.buttonSuccess]: success,
    [classes.button]: !success && !error,
    [classes.buttonError]: error,
  })

  const handleChangeValue = (event) => {
    setValue(event.target.value);
  }

  const handleChangeBridesmaid = (event) => {
    setBridesmaid(event.target.value);
  }

  const handleChangeDestiny = (event) => {
    setDestiny(event.target.value);
  }

  const handleChangeDescription = (event) => {
    setDescription(event.target.value);
  }

  const submitForm = async () => {
    setButtonLoading(true);

    let body = {
      user_id: bridesmaid,
      destiny_id: destiny,
      value: value,
      description: description,
    }

    try {
      const response = await api.post('/home', JSON.stringify(body));
      if (response.status === 201){
        setButtonLoading(false);
        setSuccess(true);
        setTimeout(redirectOnSuccess, 2000);
      }
    } catch (e) {
      setError(true);
      setButtonLoading(false);
      setTimeout(resetOnError, 1000);
    }
  }

  const resetOnError = () => {
    setError(false);
  }

  const redirectOnSuccess = () => {
    let { from } = location.state || { from: { pathname: "/payments" } };
    history.replace(from);
  }

  return (
    <main className={classes.layout}>
      <Paper className={classes.paper}>
        {
          formData.loading ? (<div className={classes.progress}><CircularProgress/></div>) : (
            <div>
              <Typography component="h1" variant="h4" align="center">
                Novo Pagamento
              </Typography>
              <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    id="value"
                    name="value"
                    label="Valor"
                    fullWidth
                    onChange={handleChangeValue}
                    value={value}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <FormControl fullWidth>
                    <InputLabel htmlFor="select">Selecione a Madrinha</InputLabel>
                    <Select id="select" name="bridesmaid" value={bridesmaid} onChange={handleChangeBridesmaid}>
                      {
                        formData.bridesmaids.map((bridesmaid) => (
                          <MenuItem key={bridesmaid.id} value={bridesmaid.id}>{bridesmaid.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <FormControl fullWidth>
                    <InputLabel htmlFor="select">Selecione o Destino</InputLabel>
                    <Select id="select" name="destiny" value={destiny} onChange={handleChangeDestiny}>
                      {
                        formData.destinys.map((destiny) => (
                          <MenuItem key={destiny.id} value={destiny.id}>{destiny.name}</MenuItem>
                        ))
                      }
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    name="description"
                    id="description"
                    label="Descrição"
                    fullWidth
                    onChange={handleChangeDescription}
                    value={description}
                  />
                </Grid>
              </Grid>
              <div className={classes.buttons}>
                <div className={classes.wrapper}>
                  <Fab
                    onClick={submitForm}
                    className={buttonClassname}
                    color="primary"
                    disabled={buttonLoading}
                  > {success && <CheckIcon/> }
                    {error && <CancelIcon/>}
                    {(!success && !error) && <SaveIcon/>}
                  </Fab>
                  {buttonLoading && <CircularProgress size={68} className={classes.fabProgress}/>}
                </div>
              </div>
              <Snackbar open={success} autoHideDuration={6000}>
                <Alert severity="success">
                  Cadastrado com sucesso
                </Alert>
              </Snackbar>
              <Snackbar open={error} autoHideDuration={6000}>
                <Alert severity="error">
                  Ocorreu um erro ao cadastrar
                </Alert>
              </Snackbar>
            </div>
          )
        }
      </Paper>
    </main>
  );
}

export default PaymentForm