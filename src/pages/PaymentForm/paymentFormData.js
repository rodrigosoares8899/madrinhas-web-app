import {useEffect, useState} from 'react';
import api from "../../shared/api";

const usePaymentFormData = function() {
  const [bridesmaids, setBridesmaids] = useState([]);
  const [destinys, setDestinys] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect( () => {
    async function loadBridesmaids() {
      const response = await api.get('/bridesmaids');
      setBridesmaids(response.data);
    }

    async function loadDestinys() {
      const response = await api.get('/destinys');
      setDestinys(response.data)
    }

    loadBridesmaids();
    loadDestinys();
    setLoading(false);

  }, []);

  return {
    bridesmaids: bridesmaids,
    destinys: destinys,
    loading: loading
  };

}

export default usePaymentFormData;
