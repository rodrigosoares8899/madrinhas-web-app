import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom';
import firebase from '../../firebase';
import {makeStyles, ThemeProvider} from '@material-ui/core/styles';
import { Container, Avatar, Icon, Typography, TextField, Grid, CircularProgress }  from '@material-ui/core';
import StyledGradientButton from '../../components/StyledGradientButton';
import myTheme from '../../Theme';

const useStyles  = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: myTheme.palette.primary.main
    },
    form:{
        width: '100%',
        marginTop: theme.spacing(3)
    },
    submit:{
        margin: theme.spacing(3, 0, 2)
    }
}));


export default function Register(){

    const history = useHistory();
    
    const [name, setName] = useState();
    const [lastName, setLastName] = useState();
    const [cpf, setCPF] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [loadingSubmit, setLoadingSubmit] = useState(false);
    
    const classes = useStyles();

    async function _handleSubmit(e){
        e.preventDefault();
        setLoadingSubmit(true);

        try {
            firebase.register(name, lastName, cpf, email, password);
            history.push("/home");
        } catch (e) {
            console.log(e);

            const errors = [];

            if (e.code === 'auth/email-already-in-use') {
                errors.push(['email', 'Esse email já está em uso']);
            } else if (e.code === 'auth/weak-password') {
                errors.push(['password', 'Senha muito fraca']);
            }
        }
    }    
    

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <Icon>person_add</Icon>
                </Avatar>
                <Typography variant='h5'>Cadastrar</Typography>
                <form type="post" className={classes.form} onSubmit={_handleSubmit}>
                    <ThemeProvider theme={myTheme}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    name="name"
                                    id="name"
                                    variant="outlined"
                                    label="Nome"
                                    required
                                    color="primary"
                                    onChange={ (e) => { setName(e.target.value) } }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    name="lastName"
                                    id="lastName"
                                    variant="outlined"
                                    label="Sobrenome"
                                    required
                                    color="primary"
                                    onChange={ (e) => { setLastName(e.target.value) } }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="cpf"
                                    id="cpf"
                                    variant="outlined"
                                    label="CPF"
                                    type="text"
                                    fullWidth
                                    required
                                    color="primary"
                                    onChange={ (e) => { setCPF(e.target.value) } }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="email"
                                    id="email"
                                    variant="outlined"
                                    label="Email"
                                    type="email"
                                    fullWidth
                                    required
                                    color="primary"
                                    onChange={ (e) => { setEmail(e.target.value) } }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="password"
                                    id="password"
                                    variant="outlined"
                                    label="Senha"
                                    type="password"
                                    fullWidth
                                    required
                                    color="primary"
                                    onChange={ (e) => { setPassword(e.target.value) } }
                                />
                            </Grid>
                        </Grid>
                    </ThemeProvider>
                   
                    <StyledGradientButton
                        className={classes.submit}
                        type="submit"
                        fullWidth={true}
                        variant="contained"
                    >
                        {
                            loadingSubmit === true ?
                            <ThemeProvider theme={myTheme}>
                                <CircularProgress size={24} color="inherit" />
                            </ThemeProvider> 
                            :
                            <div>FAZER LOGIN</div>
                        }
                          

                    </StyledGradientButton>

                    <Typography variant="body2">
                        <Link to="/">Já possui cadastro ? Fazer Login</Link>
                    </Typography>
                </form>
            </div>
        </Container>
    );
}